#include <stdio.h>
//#include "base.h"
#include "bmath.h"
#include "fill.h"
#include "hrtime.h"
#include "tool.h"
#include "ulp.h"

int INLINE func1(int a){
    printf("%d\n",a);
    a = INFINITY + a;
    printf("%d\n",a);
    return 1.0+a;
}
float INLINE func2(int a){
    return intBitsToFloat(a);
}
int64_t INLINE func3(double a){
    return doubleToRawLongBits(a);
}

double INLINE func4(double a,int b){
    return nexttowardE0(a,b);
}

int main(){
    int a = 1.0;
    uint64_t time,time1;
    COUNTCYCLE_START(1);
    time1 = Sleef_currentTimeMicros();
    func1(a);
    printf("%f\n",func2(a));
    printf("%lx\n",func3(4.9406564584124654418e-324));
    printf("%lf\n",func4(78.6666,6));
    COUNTCYCLE_END(1,time);
    time1 = Sleef_currentTimeMicros();
    printf("time : %ld \n",time1);
    return 0;
}
