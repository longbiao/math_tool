#ifndef __BMATH_H__
#define __BMATH_H__
#include <stdint.h>
#include "base.h"

typedef union
{
  uint64_t i;
  double f;
} number_double;

typedef union
{
  uint32_t i;
  float f;
} number_float;

static INLINE CONST int64_t doubleToRawLongBits(double d) {
  number_double tmp;
  tmp.f = d;
  return tmp.i;
}

static INLINE CONST double longBitsToDouble(int64_t i) {
  number_double tmp;
  tmp.i = i;
  return tmp.f;
}

static INLINE CONST int32_t floatToRawIntBits(float d) {
  number_float tmp;
  tmp.f = d;
  return tmp.i;
}

static INLINE CONST float intBitsToFloat(int32_t i) {
  number_float tmp;
  tmp.i = i;
  return tmp.f;
}

#ifndef MIN
    #define MIN(x, y) ((x) < (y) ? (x) : (y))
#endif

#ifndef MAX
    #define MAX(x, y) ((x) > (y) ? (x) : (y))
#endif

#ifndef ABS
    #define ABS(x) ((x) < 0 ? -(x) : (x))
#endif

#define DENORMAL_DBL_MIN (4.9406564584124654418e-324)
#define POSITIVE_INFINITY INFINITY
#define NEGATIVE_INFINITY (-INFINITY)
#define DENORMAL_FLT_MIN (1.4012984643248170709e-45f)
#define POSITIVE_INFINITYf ((float)INFINITY)
#define NEGATIVE_INFINITYf (-(float)INFINITY)

#define DENORMAL_MIN_float (1.4012984643248170709e-45f)
#define DENORMAL_MIN_double (4.9406564584124654418e-324)
#define POSITIVE_INFINITY_float ((float)INFINITY)
#define NEGATIVE_INFINITY_float (-(float)INFINITY)
#define POSITIVE_INFINITY_double INFINITY
#define NEGATIVE_INFINITY_double (-INFINITY)
#endif