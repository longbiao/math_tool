#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
// this c code is used by some code which produced by an autotool.
#define STRING_SIZE 50
#define ALG_DES_TYPE_SIZE 20
// this Marco is the max size of algorithm description type which 
// support by the tool.
static const int funcInfoObject_set_stat_normal = 0;
static const int funcInfoObject_set_stat_warrning = -1;
static const int funcInfoObject_set_stat_error = -2;
static const int funcInfoObject_set_stat_detail_hah = -3;
static const int adsa_flag_add = 0;
static const int adsa_flag_reset = 1;
#define QP 128
#define DP 64
#define SP 32
#define HP 16

typedef struct {
    char library_name[STRING_SIZE];
    char library_version_major;
    char library_version_minor;
    char library_version_patchlevel;
    char library_is_unkown_library_version;
    char simd_vector[STRING_SIZE];
    int  simd_vector_length;
    int  floating_point_size;
    char operation[STRING_SIZE];
    char algorithmDescriptionStringArray[ALG_DES_TYPE_SIZE][STRING_SIZE];
    short algorithmAcc;//don`t change this attr manually
    float precision;
    char extra_info[STRING_SIZE];
}funcInfoObject;

static inline int funcInfoObject_change_algorithm_description_string_array(funcInfoObject *funcObj, char flag, \
char *description){   
    char stat = 0;
    int position = (flag == adsa_flag_add ? funcObj->algorithmAcc : 0);
    int length = strlen(description) + 1;
    char *buffer = (char *)malloc(sizeof(char) * length);
    strncpy(buffer, description, length);//
    buffer[length - 1] = 0;
    char sep[] = "-";    
    char *p = strtok(buffer, sep);
    while(p != NULL){
        if(position >= ALG_DES_TYPE_SIZE) {
            stat = -1;// report warning!
            break;
        }
        strncpy(funcObj->algorithmDescriptionStringArray[position], p, STRING_SIZE);
        funcObj->algorithmDescriptionStringArray[position][STRING_SIZE - 1] = 0;
        p = strtok(NULL, sep);
        position ++;
    }
    funcObj->algorithmAcc = position;
    free(buffer);
    return stat;
}

// get initialized funcInfoObject 
static funcInfoObject funcInfoObject_init(char *library_name, char library_version_major, \
    char library_version_minor, char library_version_patchlevel, char *simd_vector, \
    int simd_vector_length, int floating_point_size, char *operation, char *algorithmDescriptionString, \
    float precision, char *extra_info){
    funcInfoObject funcObj;
    int tmpsize1 = STRING_SIZE - 1;
    strncpy(funcObj.library_name, library_name, tmpsize1);
    funcObj.library_name[tmpsize1] = 0;
    funcObj.library_version_major = library_version_major;
    funcObj.library_version_minor = library_version_minor;
    funcObj.library_version_patchlevel = library_version_patchlevel;
    strncpy(funcObj.simd_vector, simd_vector, tmpsize1);
    funcObj.simd_vector[tmpsize1] = 0;
    funcObj.simd_vector_length = simd_vector_length;
    funcObj.floating_point_size = floating_point_size;
    strncpy(funcObj.operation,operation,tmpsize1);
    funcObj.operation[tmpsize1] = 0;
    funcObj.algorithmAcc = 0;
    if(algorithmDescriptionString != NULL){
        funcInfoObject_change_algorithm_description_string_array(&funcObj, adsa_flag_reset, \
        algorithmDescriptionString);
    }
    funcObj.precision = precision;
    if(extra_info != NULL){
        strncpy(funcObj.extra_info, extra_info, tmpsize1);
        funcObj.extra_info[tmpsize1] = 0;
    }else{
        strncpy(funcObj.extra_info, "NULL", tmpsize1);
        funcObj.extra_info[tmpsize1] = 0;
    }
    return funcObj;
}

// get initialized funcInfoObject by format string.
// the fomat of the string :
// [library_name-version_major.version_minor.version_patchlevel] [simd_vector(simd_vector_length)] [operation] 
// [Uprecision] [floating_point_size] [algorithmDescriptionString1-algorithmDescriptionString2-.....] [extra_info]
// notice :
//     # " [ ] - . U  " is special character
//     # floating_point_size : can be decimal number or [QP|DP|SP|HP]
//     # extra_info : can be "null" or "NULL"
//     # algorithmDescriptionString : can be "null" or "NULL"
// carefully use this funciton!
// beta version!
static const bool lookup_mode_normal = true;
static const bool lookup_mode_reverse = false;
static inline char* lookup_char_with_prohibit_char(char *src, char det, char *prohibit_char, bool mode, int *stat){
    char *p1,*p2;
    p1 = strchr(src, det);
    if(mode){
        p2 = strbrk(src, prohibit_char);
    }else{
        p2 = strspnp(src, prohibit_char);
    }
    if(p1 == NULL){
        *stat = -1; // cant`t find det !
        return NULL;
    }
    if(p2 == NULL){
        *stat = 0; // cant`t find prohibit_char.
        return p1; // systenx is correct.
    }
    if(p2 < p1){
        *stat = -2; // prohibit_char is before det!
        return p2; // return the sytnax error position.
    }
    *stat = 0;
    return p1;
}
static funcInfoObject funcInfoObject_init_by_string(char *init_string, int *stat){
    // how to handle the sytnax error?
    funcInfoObject funcObj;
    int length = strlen(init_string);
    char *buffer = (char *)malloc(sizeof(char) * (length + 1));
    strncpy(buffer, init_string,length);
    char amount_attr = 0; // normal : 7
    char *p1, *p2, *tmpbuffer;
    int tmplen = 0;
    p1 = lookup_char_with_prohibit_char(buffer, '[', " ", lookup_mode_reverse, stat);
    p1 ++;
    if(*stat != 0) return funcObj;
    p2 = lookup_char_with_prohibit_char(p1, ']', "[", lookup_mode_normal, stat);
    if(*stat != 0) return funcObj;
    tmplen = p2 - p1;
    {
        tmpbuffer = (char *)malloc(sizeof(char) * (tmplen + 1));
        strncpy(tmpbuffer, p1, tmplen);
        tmpbuffer[tmplen - 1] = 0;
        // handle library_info in here @@@@TBST@@@@
        char *p3 = strchr(tmpbuffer, '-');

        tmplen = p3 - tmpbuffer;
        tmplen = min(tmplen, STRING_SIZE);
        strncpy(funcObj.library_name, tmpbuffer, tmplen);
        funcObj.library_name[tmplen - 1] = 0;
        
        free(tmpbuffer);
    }
    *stat = amount_attr == 7 ? funcInfoObject_set_stat_normal : (amount_attr == 0 ? funcInfoObject_set_stat_error :\
    funcInfoObject_set_stat_warrning);
    return funcObj;
}


static const char* string_no_value = NULL;
static const char  char_no_value = -1;
static const int   int_no_value = -1;
static const float float_no_value = -1.0;
//when don`t want to change some attr of funcObj in funcObj set function,use above this value. 
static int funcInfoObject_set_all(funcInfoObject *funcObj, char *library_name, char library_version_major, \
char library_version_minor, char library_version_patchlevel, char *simd_vector, int simd_vector_length, \
int floating_point_size, char *operation, char *algorithmDescriptionString, float precision, char *extra_info){
    int tmpsize1 = STRING_SIZE - 1;
    if(library_name != string_no_value) {
       strncpy(funcObj->library_name ,library_name,tmpsize1);
       funcObj->library_name[tmpsize1] = 0; 
    }
    if(library_version_major != char_no_value){
       funcObj->library_version_major = library_version_major; 
    }
    if(library_version_minor != char_no_value){
        funcObj->library_version_minor = library_version_minor;
    }
    if(library_version_patchlevel != char_no_value){
        funcObj->library_version_patchlevel = library_version_patchlevel;
    }
    if(simd_vector != string_no_value){
        strncpy(funcObj->simd_vector, simd_vector, tmpsize1);
        funcObj->simd_vector[tmpsize1] = 0;
    }
    if(simd_vector_length != int_no_value){
        funcObj->simd_vector_length = simd_vector_length;
    }
    if(floating_point_size != int_no_value){
        funcObj->floating_point_size = floating_point_size;
    }
    if(operation != string_no_value){
        strncpy(funcObj->operation, operation, tmpsize1);
        funcObj->operation[tmpsize1] = 0;
    }
    if(algorithmDescriptionString != string_no_value){
        return funcInfoObject_change_algorithm_description_string_array(funcObj, adsa_flag_reset, \
        algorithmDescriptionString);
    }
    if(precision != float_no_value){
        funcObj->precision = precision;
    }
    if(extra_info != string_no_value){
        strncpy(funcObj->extra_info, extra_info, tmpsize1);
        extra_info[tmpsize1] = 0;
    }
    return funcInfoObject_set_stat_normal; 
}

//frequently used
static int funcInfoObject_set_simple(funcInfoObject *funcObj, char *simd_vector, int simd_vector_length, \
int floating_point_size, char *operation, float precision){
    int tmpsize1 = STRING_SIZE - 1;
    if(simd_vector != string_no_value){
        strncpy(funcObj->simd_vector, simd_vector, tmpsize1);
        funcObj->simd_vector[tmpsize1] = 0;
    }
    if(simd_vector_length != int_no_value){
        funcObj->simd_vector_length = simd_vector_length;
    }
    if(floating_point_size != int_no_value){
        funcObj->floating_point_size = floating_point_size;
    }
    if(operation != string_no_value){
        strncpy(funcObj->operation, operation, tmpsize1);
        funcObj->operation[tmpsize1] = 0;
    }
    if(precision != float_no_value){
        funcObj->precision = precision;
    }
    return funcInfoObject_set_stat_normal;
}

//library info
static int funcInfoObject_set_library_info(funcInfoObject *funcObj, char* library_name,int library_version_major,\
int library_version_minor, int library_version_patchlevel){
    int tmpsize1 = STRING_SIZE - 1;
    if(library_name != string_no_value) {
       strncpy(funcObj->library_name ,library_name,tmpsize1);
       funcObj->library_name[tmpsize1] = 0; 
    }
    if(library_version_major != char_no_value){
       funcObj->library_version_major = library_version_major; 
    }
    if(library_version_minor != char_no_value){
        funcObj->library_version_minor = library_version_minor;
    }
    if(library_version_patchlevel != char_no_value){
        funcObj->library_version_patchlevel = library_version_patchlevel;
    }
    return funcInfoObject_set_stat_normal;
}

//reset the algorithmDescriptionString.
static int funcInfoObject_reset_algorithm_description(funcInfoObject *funcObj,char *algorithmDescriptionString){
    return funcInfoObject_change_algorithm_description_string_array(funcObj, adsa_flag_reset, \
    algorithmDescriptionString);
}

//clear all algorithmDescriptionStringArray of the funcInfoObject.
static int funcInfoObject_clear_algorithm_description(funcInfoObject *funcObj){
    funcObj->algorithmAcc = 0;
    return funcInfoObject_set_stat_normal;
}

//add another algorithm Description for the funcInfoObject 
static int funcInfoObject_add_algorithm_description(funcInfoObject *funcObj,char *algorithmDescriptionString){
    if(funcObj->algorithmAcc >= ALG_DES_TYPE_SIZE){
        return funcInfoObject_set_stat_error;
    }
    return funcInfoObject_change_algorithm_description_string_array(funcObj, adsa_flag_reset, \
    algorithmDescriptionString);
}

// detail setting.
// library name
static int funcInfoObject_set_library_name(funcInfoObject *funcObj,char *library_name){
    if(library_name != string_no_value){
        strncpy(funcObj->library_name, library_name, (STRING_SIZE -2));
        funcObj->library_name[(STRING_SIZE - 1)] = 0;
    }
    return funcInfoObject_set_stat_normal;
}

//library version
static int funcInfoObject_set_library_version(funcInfoObject *funcObj, char library_version_major, char \
    library_version_minor,char library_version_patchlevel,char library_is_unkown_library_version){
    if(library_version_major != char_no_value){
       funcObj->library_version_major = library_version_major; 
    }
    if(library_version_minor != char_no_value){
        funcObj->library_version_minor = library_version_minor;
    }
    if(library_version_patchlevel != char_no_value){
        funcObj->library_version_patchlevel = library_version_patchlevel;
    }
    return funcInfoObject_set_stat_normal;
}

// simd vector and the length of vector
static int funcInfoObject_set_simd_vector(funcInfoObject *funcObj, char *simd_vector, int simd_vector_length){
    int tmpsize1 = STRING_SIZE - 1;
    if(simd_vector != string_no_value){
        strncpy(funcObj->simd_vector, simd_vector, tmpsize1);
        funcObj->simd_vector[tmpsize1] = 0;
    }
    if(simd_vector_length != int_no_value){
        funcObj->simd_vector_length = simd_vector_length;
    }
    return funcInfoObject_set_stat_normal;
}

//floating-point size
static int funcInfoObject_set_floating_point_size(funcInfoObject *funcObj, int floating_point_size, \
char *description){
    if(floating_point_size != int_no_value){
        funcObj->floating_point_size = floating_point_size;
    }
    return funcInfoObject_set_stat_normal;
}

//operation 
static int funcInfoObject_set_operation(funcInfoObject *funcObj, char *operation){
    int tmpsize1 = STRING_SIZE - 1;
    if(operation != string_no_value){
        strncpy(funcObj->operation, operation, tmpsize1);
        funcObj->operation[tmpsize1] = 0;
    }
    return funcInfoObject_set_stat_normal;
}

//precision
static int funcInfoObject_set_precision(funcInfoObject *funcObj, float precision){
    if(precision != float_no_value){
        funcObj->precision = precision;
    }
    return funcInfoObject_set_stat_normal;
}

//extra info
static int funcInfoObject_set_extra_info(funcInfoObject *funcObj, char *extra_info){
    int tmpsize1 = STRING_SIZE - 1;
    if(extra_info != string_no_value){
        strncpy(funcObj->extra_info, extra_info, tmpsize1);
        extra_info[tmpsize1] = 0;
    }
    return funcInfoObject_set_stat_normal;
}

//print with no format
static int funcInfoObject_print_raw(funcInfoObject *funcObj){
    printf("library : %s-%d.%d.%d\n", funcObj->library_name, funcObj->library_version_major, \
    funcObj->library_version_minor, funcObj->library_version_patchlevel);
    printf("simd : %s(W%d)\n", funcObj->simd_vector, funcObj->simd_vector_length);
    printf("operation : %s, precision : U%g, floating-point-size : %d\n", funcObj->operation, \
    funcObj->precision, funcObj->floating_point_size);
    printf("algorithm description : ");
    if(funcObj->algorithmAcc == 0){
        printf("NULL");
    }
    int i = 0;
    for(i = 0 ; i < funcObj->algorithmAcc ; i++){
        if(i == 0){
            printf("%s",funcObj->algorithmDescriptionStringArray[i]);
        }else{
            printf("-%s",funcObj->algorithmDescriptionStringArray[i]);
        }       
    }
    printf("\n");
    return funcInfoObject_set_stat_normal;
}

//print with format
static int funcInfoObject_print_format(funcInfoObject *funcObj){
    char tmpstring2[5];
    switch(funcObj->floating_point_size){
    case QP:
        strncpy(tmpstring2, "QP",5);
        break;
    case DP:
        strncpy(tmpstring2, "DP",5);
        break;
    case SP:
        strncpy(tmpstring2, "SP",5);
        break;
    case HP:
        strncpy(tmpstring2, "HP",5);
        break;
    default:
        snprintf(tmpstring2, 5,"%d", funcObj->floating_point_size); 
    }
    tmpstring2[4] = 0;
    if(funcObj->algorithmAcc == 0){
        printf("[%s-%d.%d.%d] [%s(W%d)] [%s] [U%g] [%s]\n",funcObj->library_name,funcObj->\
        library_version_major, funcObj->library_version_minor, funcObj->library_version_patchlevel, \
        funcObj->simd_vector, funcObj->simd_vector_length, funcObj->operation, funcObj->precision, tmpstring2);
    }
    else if(funcObj->algorithmAcc <= 5){
        int i = 0;
        int len = 0;
        for(i = 0; i < funcObj->algorithmAcc; i++){
            len += strlen(funcObj->algorithmDescriptionStringArray[i]);
        }
        len += funcObj->algorithmAcc + 1;
        char *tmpstring1 = (char *)malloc(sizeof(char)*(len));
        for(i = 0;i < (len);i++){
            tmpstring1[i] = 0;
        }
        for(i = 0; i < funcObj->algorithmAcc; i++){
            if(i == 0){
                strcat(tmpstring1, funcObj->algorithmDescriptionStringArray[i]);
            }else{
                strcat(tmpstring1, "-");
                strcat(tmpstring1, funcObj->algorithmDescriptionStringArray[i]);
            }
        }
        tmpstring1[len-1] = 0; 
        if(funcObj->algorithmAcc <= 3){
            printf("[%s-%d.%d.%d] [%s(W%d)] [%s] [U%g] [%s] [%s]",funcObj->library_name,funcObj->\
            library_version_major, funcObj->library_version_minor, funcObj->library_version_patchlevel, \
            funcObj->simd_vector, funcObj->simd_vector_length, funcObj->operation, funcObj->precision, tmpstring2, \
            tmpstring1);
        }else{
            printf("[%s-%d.%d.%d] [%s(W%d)] [%s] [U%g] [%s] \n[%s]",funcObj->library_name,funcObj->\
            library_version_major, funcObj->library_version_minor, funcObj->library_version_patchlevel, \
            funcObj->simd_vector, funcObj->simd_vector_length, funcObj->operation, funcObj->precision, tmpstring2, \
            tmpstring1);
        }
        printf("\n");
        free(tmpstring1);
    }else{
            printf("unreadly!");//@TBW
    }
    return funcInfoObject_set_stat_normal;
}
