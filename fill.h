#ifndef __FILL_H__
#define __FILL_H__
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "bmath.h"
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

static const int flag_right = -1; // ????????????????????
static const int flag_left  = 1;  // ????????????????????
float static INLINE bool2pn(int boolv){
    int32_t tmp = ((boolv & 0x1) << 31) ^ 0x3f800000;
    return intBitsToFloat(tmp);
}
//#define PN_ON
#ifdef PN_ON
    float static INLINE return_signed(int pn){return bool2pn(pn);}
    #define CUM_RANDOM() ({srandom(time(NULL)+random_sed);random();})
    #define MY_MARC() ({\
        pn = random_value & 0x1;\
        random_value = random_value >> 1;\
        random_value = (i & 0x3F) ? random_value : random_value_bk; /* niter % 64 == 0;*/\
    })
#else
    float static INLINE return_signed(int pn){return 1.0;}
    #define CUM_RANDOM() ({1;})
    #define MY_MARC()
#endif

//#define NEBOUNDARY
#ifdef NEBOUNDARY
    #define HANDLE_BOUNDARY(bstart,bstep) (bstart + bstep)
    #define HANDLE_BOUNDARY1(z) ({if (z == 0) z = 1;})
#else
    #define HANDLE_BOUNDARY(bstart,bstep) (bstart)
    #define HANDLE_BOUNDARY1(z)
#endif


void static INLINE fill_single_double(double *buf,int niter,double value,int random_sed){
  int i,pn;
  uint64_t random_value,random_value_bk;
  random_value    = CUM_RANDOM();
  random_value_bk = random_value;

  for(i = 0;i < niter;i++){
    MY_MARC();
    buf[i] = value * (double)return_signed(pn);
  }
}

void static INLINE fill_single_float (float *buf, int niter,float  value,int random_sed){
  int i, pn;
  uint64_t random_value,random_value_bk;
  random_value    = CUM_RANDOM();
  random_value_bk = random_value;

  for(i = 0;i < niter;i++){
    MY_MARC();
    buf[i] = value * return_signed(pn);
  }
}

void static INLINE fill_bit_double(double *buf,int niter,double min,double max,int random_sed){
//只适用于正数,按位来取随机数;
    srandom(time(NULL)+random_sed);
    number_double min_number,max_number;
    min_number.f = min;
    max_number.f = max;
    uint64_t min_bit = min_number.i;
    uint64_t max_bit = max_number.i;
    number_double r;
    uint64_t r_i;
    uint64_t random_value;
    int i;
    for(i = 0 ;i < niter ;i++){
        random_value = random();
        r.i = random_value % ( max_bit - min_bit + 1) + min_bit;
        buf[i] = r.f * (double)return_signed(random_value);
    }
}

void static INLINE fill_bit_float(float *buf,int niter,float min,float max,int random_sed){
//只适用于正数,按位来取随机数;
    srandom(time(NULL)+random_sed);
    number_float min_number,max_number;
    min_number.f = min;
    max_number.f = max;
    uint32_t min_bit = min_number.i;
    uint32_t max_bit = max_number.i;
    number_float r;
    uint32_t r_i;
    uint64_t random_value;
    int i;
    for(i = 0;i < niter ;i++){
        random_value = random();
        r.i = random_value % ( max_bit - min_bit + 1) + min_bit;
        buf[i] = r.f * (float)return_signed(random_value);
    }
}

void static INLINE fill_double(double *buf,int niter ,double min, double max, int random_sed) {
  srandom(time(NULL));
  uint64_t random_value;
  int i;
  for(i=0;i<niter;i++) {
    random_value = random();
    double r = ((double)random_value + RAND_MAX * (double)random_value) / (RAND_MAX * (double)RAND_MAX);
    buf[i] = (r * (max - min) + min) * (double)return_signed(random_value);
  }
}

void static INLINE fill_float(float* buf,int niter ,double min, double max, int random_sed) {
  srandom(time(NULL));
  uint64_t random_value;
  int i;
  for(i=0;i<niter;i++) {
    random_value = random();
    double r = ((double)random_value + RAND_MAX * (double)random_value) / (RAND_MAX * (double)RAND_MAX);
    buf[i] = (float)(r * (max - min) + min) * (float)return_signed(random_value);
  }
}

static double nexttowardE0(double x, int n) {
  union {
    double f;
    uint64_t u;
  } cx;
  cx.f = x;
  cx.u -=n ;
  return cx.f;
}
static float nexttowardE0f(float x, int n) {
  union {
    float f;
    int32_t u;
  } cx;
  cx.f = x;
  cx.u -= n;
  return x == 0 ? 0 : cx.f;
}

static double nexttoward0(double x, int n , int flag) {
  union {
    double f;
    uint64_t u;
  } cx;
  HANDLE_BOUNDARY1(n);
  n = n * flag;
  cx.f = x;
  cx.u -=n ;
  return cx.f;
}

static float nexttoward0f(float x, int n,int flag) {
  union {
    float f;
    int32_t u;
  } cx;
  HANDLE_BOUNDARY1(n);
  n = n * flag;
  cx.f = x;
  cx.u -= n;
  return x == 0 ? 0 : cx.f;
}

#define rnd_someone_double(target,flag)({ nexttoward0( target,RAND_MAX & ((1 << (random() & 31)) - 1),flag);})
#define rnd_someone_float(target,flag) ({nexttoward0f( target,RAND_MAX & ((1 << (random() & 15)) - 1),flag);})

static void fill_toward_boundary_double(double *buf,int niter,double min ,double max,int random_sed) {
  srandom(time(NULL)+random_sed);
  int i;
  int flag;
  int flag2;
  double target;
  uint64_t random_value;
  for(i=0;i<niter;i++) {
    random_value = random();
    flag  = random_value & 1;
    target= flag ? min : max; 
    flag  = flag ? flag_right:flag_left; 
    buf[i]= rnd_someone_double(target,flag) * (double)return_signed(random_value >> 1);
  }
}

static void fill_toward_boundary_float(float *buf,int niter,float min ,float max,int random_sed) {
  srandom(time(NULL)+random_sed);
  int i;
  int flag;
  int flag2;
  float target;
  float tmp;
  uint64_t random_value;
  for(i=0;i<niter;i++) {
    random_value = random();
    flag  = random_value & 1;
    target= flag ? min : max; 
    flag  = flag ? flag_right:flag_left;
    tmp = rnd_someone_float(target,flag) * return_signed(random_value >> 1);
    buf[i]= tmp;
  }
}

static void fill_random_float(float *array,int length,float *range_array,int range_array_length,int random_sed){
    float rnd;
    float max,min;
    srandom(random_sed);
    int i;
    int index;
    uint64_t random_value;
    for(i=0;i<length;i++){
      random_value = random();
      index = random_value % (range_array_length-1);
      min   = range_array[index];
      max   = range_array[index+1];
      rnd   = ((float)random_value + RAND_MAX * (float)random_value) / (RAND_MAX * (float)RAND_MAX);
      rnd   = rnd * (max - min) + min;
      array[i] = rnd * return_signed(random_value);
    }
}

static void fill_random_double(double *array,int length,double *range_array,int range_array_length,int random_sed){
    double rnd;
    double max,min;
    srandom(random_sed);
    int i;
    int index;
    uint64_t random_value;
    for(i=0;i<length;i++){
      random_value = random();
      index = random_value % (range_array_length-1);
      min   = range_array[index];
      max   = range_array[index+1];
      rnd   = ((double)random_value + RAND_MAX * (double)random_value) / (RAND_MAX * (double)RAND_MAX);
      rnd   = rnd * (max - min) + min;
      array[i] = rnd * return_signed(random_value);
    }
}
void fill_toward_point_double(double *buf,int niter,double point,int random_sed){
  srandom(time(NULL)+random_sed);
  int i,flag;
  uint64_t random_value;
  for(i = 0;i < niter;i++){
    random_value = random();
    flag = (int)(((int32_t)(((int32_t)random_value & 0x1) << 31) >> 31) ^ 0x1);
    buf[i] = rnd_someone_double(point,flag);
  }
}

void fill_toward_point_float(float *buf,int niter,float point,int random_sed){
  srandom(time(NULL)+random_sed);
  int i,flag;
  uint64_t random_value;
  for(i = 0;i < niter;i++){
    random_value = random();
    flag = (int)(((int32_t)(((int32_t)random_value & 0x1) << 31) >> 31) ^ 0x1);
    buf[i] = rnd_someone_float(point,flag);
  }
}
#define MAX_DOUBLE_STRING_LENGTH 20
#define MAX_FLOAT_STRING_LENGTH  40
size_t fillSpecialValueFromFile_double(int fd,size_t offset,double *targetArray,size_t targetArrayLength,int64_t *stat){
   size_t currentTargetArrayPosition = 0;
   char *buffer;
   size_t bufferSize;
   int64_t tmpStat = 0,usedChar = 0;
   size_t oriPosition = offset;
   bufferSize = (targetArrayLength < MAX_DOUBLE_STRING_LENGTH) ? 80 * targetArrayLength : 80 * MAX_DOUBLE_STRING_LENGTH;
   buffer = (char *)malloc(bufferSize * sizeof(char));
   while(currentTargetArrayPosition < targetArrayLength){
        tmpStat = pread(fd,buffer,bufferSize,offset);
        if(tmpStat == -1) { *stat = tmpStat;return currentTargetArrayPosition;}
        if(tmpStat ==  0) { *stat = usedChar;return currentTargetArrayPosition;}
        buffer[tmpStat] = '\0';

        char sym = ',';
        char *tmp = strrchr(buffer,sym);
        int64_t a = (int64_t)(tmp - buffer);
        buffer[a] = '\0';

        char *delim = ",";
        char *p = strtok(buffer,delim);
        while(p != NULL){
            if(currentTargetArrayPosition < targetArrayLength){
                targetArray[currentTargetArrayPosition] = strtod(p,NULL);
                /*
                if(errno == ERANGE){
                    printf("n%sahah\n",p);
                    printf("%.128f\n",targetArray[currentTargetArrayPosition]);
                    perror("strtod aa");
                    close(fd);
                    exit(EXIT_FAILURE);
                }
                */
                usedChar += (strlen(p) + 1);
                currentTargetArrayPosition ++;
                p = strtok(NULL,delim);
            }else{
                break;
            }

        }
        offset = oriPosition + usedChar;
   }
   free(buffer);
   *stat = usedChar;
   return currentTargetArrayPosition;
}

static const int mode_point = 0;
static const int mode_toward = 1;
static const int mode_range = 2;
static int fill_random_bymix_double(double *sourceArray,int sourceArray_length,double *targetArray,int targetArray_length,int random_sed){
  int i,j,k,random_value,mode,index,flag;
  srandom(time(NULL)+random_sed);
  double cur,next,result,tmp;
  for(i=0;i<targetArray_length;i++){
    random_value = random();
    mode = random_value % 0x3;
    random_value = random();// need add random
    index = random_value % (sourceArray_length-1);
    cur = sourceArray[index];
    next= sourceArray[index+1];
    if(isnan(cur)) mode=mode&0;
    if(mode==mode_point) {
      result = cur * return_signed(random_value);
    }
    if(mode==mode_toward) {
      flag = (int)(((int32_t)(((int32_t)random_value & 0x1) << 31) >> 31) ^ 0x1);
      result = rnd_someone_double(cur,flag);
      result = result * return_signed(random_value);
    }
    if(mode==mode_range) {
      tmp = ((double)random_value + RAND_MAX * (double)random_value) / (RAND_MAX * (double)RAND_MAX);
      result = (tmp * (next - cur) + cur) * (double)return_signed(random_value);
    }
    //printf("%10.12f\n",result); //nan出现的频率太大了!
    targetArray[i]=result;
  }
  return targetArray_length;
}
static int fill_random_bymix_float(float *sourceArray,int sourceArray_length,float *targetArray,int targetArray_length,int random_sed){
  int i,j,k,random_value,random_value1,random_value2,mode,index,flag;
  srandom(time(NULL)+random_sed);
  float cur,next,result,tmp;
  for(i=0;i<targetArray_length;i++){
    random_value = random();
    mode = random_value % 0x3;
    random_value1 = random();
    random_value2 = random();
    index = random_value1 % (sourceArray_length-1);
    cur = sourceArray[index];
    next= sourceArray[index+1];
    if(isnan(cur)) mode=mode&0;
    if(mode==mode_point) {
      result = cur * return_signed(random_value2);
    }
    if(mode==mode_toward) {
      flag = (int)(((int32_t)(((int32_t)random_value & 0x1) << 31) >> 31) ^ 0x1);
      result = rnd_someone_float(cur,flag);
      result = result * return_signed(random_value2);
    }
    if(mode==mode_range) {
      tmp = ((float)random_value + RAND_MAX * (float)random_value) / (RAND_MAX * (float)RAND_MAX);
      result = (tmp * (next - cur) + cur) * (float)return_signed(random_value2);
    }
    //printf("%10.12f\n",result); //nan出现的频率太大了! 其他的数出现的次数也不平衡
    targetArray[i]=result;
  }
  return targetArray_length;
}

static void fill_double1(double *buf,int length, double min, double max) {
  srandom(time(NULL));
  for(int i=0;i<length;i++) {
    double r = ((double)random() + RAND_MAX * (double)random()) / (RAND_MAX * (double)RAND_MAX);
    buf[i] = r * (max - min) + min;
  }
}

static void fill_float1(float   *buf,int length, double min, double max) {
  srandom(time(NULL));
  for(int i=0;i<length;i++) {
    double r = ((double)random() + RAND_MAX * (double)random()) / (RAND_MAX * (double)RAND_MAX);
    buf[i] = r * (max - min) + min;
  }
}

#define fillByOne(array,array_length,point,random_sed,vartype,filltype)({\
    fill_toward_point_##vartype(array,array_length,point,random_sed);\
})

#define fillByTwo(array,array_length,min,max,random_sed,vartype,filltype) ({\
  fill_##filltype(array,array_length,min,max,random_sed,vartype);\
})

#define fill_(array,array_length,min,max,random_sed,type) ({\
  fill_##type(array,array_length,min,max,random_sed);\
})

#define fill_TR(array,array_length,min,max,random_sed,type) ({\
  fill_toward_boundary_##type(array,array_length,min,max,random_sed);\
})

#define fill_MIXR(array,array_length,min,max,random_sed,type) ({\
  fill_random_##type(array,array_length,min,max,random_sed);\
})

#endif
