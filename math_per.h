#include "fill.h"
#include "tool.h"

#if (defined(__GNUC__) || defined(__CLANG__)) && (defined(__i386__) || defined(__x86_64__))
#include <x86intrin.h>
#endif
#if (defined(_MSC_VER))
#include <intrin.h>
#endif
#if defined(__ARM_NEON__) || defined(__ARM_NEON)
#include <arm_neon.h>
#endif

#pragma GCC optimize ("O0") // This is important
#ifndef SVMLULP
#define SVMLULP
#endif
#ifndef MY_VECLEN
#define MY_VECLEN 8
#endif

//the last args must be empty  or 'TR'(toward_range_boundary) or 'MIXR'(mix the range boundary)
#define callFuncLibm1_1(funcName, arg, type,lib,operation,float_precision,vectorInstruction,VerctorWidth,ulp,start,end,type2,...) ({				\
      fillByTwo(arg,niter*veclen,start,end,random_sed++,type2,__VA_ARGS__);\
      uint64_t t = Sleef_currentTimeMicros();	\
      type acc = 0;int i=0;int j=0;	\
      for(j=0;j<niter2;j++) {	\
        type *p = (type *)(arg);\
        for(i=0;i<niter1;i++) acc += funcName(*p++);\
      }	\
      t=Sleef_currentTimeMicros() - t;\
      int count = (sizeof(type)/sizeof(type2));\
      printf(#funcName","#lib","#operation","#float_precision","#vectorInstruction","#VerctorWidth","#ulp","#start"~"#end",%g\n",(double)(t)/(niter*count));\
      fflush(stdout);\
      acc;\
    })

#define callFuncLibm1_2(funcName, arg1, arg2, type,lib,operation,float_precision,vectorInstruction,VerctorWidth,ulp,start1,end1,start2,end2,type2,...) ({\
      fillByTwo(arg1,niter*veclen,start1,end1,random_sed++,type2,__VA_ARGS__);fillByTwo(arg2,niter*veclen,start2,end2,random_sed++,type2,__VA_ARGS__);\
      uint64_t t = Sleef_currentTimeMicros();int i=0;int j=0;\
      for(j=0;j<1;j++) {\
        type *p1 = (type *)(arg1), *p2 = (type *)(arg2);\
        for(i=0;i<niter;i++) acc += funcName(*p1++, *p2++);\
      }\
      t=Sleef_currentTimeMicros() - t;\
      int count = (sizeof(type)/sizeof(type2));\
      printf(#funcName","#lib","#operation","#float_precision","#vectorInstruction","#VerctorWidth","#ulp","#start1"~"#end1"&"#start2"~"#end2",%g\n",(double)(t)/(niter*count));\
      fflush(stdout);\
      acc;\
    })

//the last args should be empty
#define callFuncLibmToPoint1_1(funcName,arg,type,lib,operation,float_precision,vectorInstruction,VerctorWidth,ulp,point,type2,...) ({\
      fillByOne(arg,niter*veclen,point,random_sed++,type2);\
      uint64_t t = Sleef_currentTimeMicros();       \
      type acc = 0;int i=0;int j=0;         \
      for(j=0;j<niter2;j++) {         \
        type *p = (type *)(arg);          \
        for(i=0;i<niter1;i++) acc += funcName(*p++);    \
      } \
      t=Sleef_currentTimeMicros() - t;\
      int count = (sizeof(type)/sizeof(type2));\
      printf(#funcName","#lib","#operation","#float_precision","#vectorInstruction","#VerctorWidth","#ulp","#point",%g\n",(double)(t)/(niter*count));\
      fflush(stdout);\
      acc;\
    })

#define callFuncLibmToPoint1_2(funcName, arg1, arg2, type,lib,operation,float_precision,vectorInstruction,VerctorWidth,ulp,point1,point2,...) ({\
      fillByOne(arg1,niter*veclen,point1,random_sed++,type2);fillByOne(arg2,niter*veclen,point2,random_sed++,type2);\
      uint64_t t = Sleef_currentTimeMicros();int i=0;int j=0;                           \
      for(j=0;j<niter2;j++) {\
        type *p1 = (type *)(arg1), *p2 = (type *)(arg2);                \
        for(i=0;i<niter1;i++) acc += funcName(*p1++, *p2++);           \
      }                                                                 \
      t=Sleef_currentTimeMicros() - t;\
      int count = (sizeof(type)/sizeof(type2));\
      printf(#funcName","#lib","#operation","#float_precision","#vectorInstruction","#VerctorWidth","#ulp","#point1","#point2",%g\n",(double)(t)/(niter*count));\
      fflush(stdout);                                                   \
      acc;                                                   \
    })

#define callFuncSleef1_1(funcName, arg, type,lib,operation,float_precision,vectorInstruction,VerctorWidth,ulp,start,end,type2,...) ({ \
      fillByTwo(arg,niter*veclen,start,end,random_sed++,type2,__VA_ARGS__);					\
      uint64_t t = Sleef_currentTimeMicros();int i=0;int j=0;           \
      for(j=0;j<niter2;j++) {                                   \
        type *p = (type *)(arg);                                        \
        for(i=0;i<niter1;i++) funcName(*p++);                   \
      }                                                                 \
      t=Sleef_currentTimeMicros() - t;\
      int count = (sizeof(type)/sizeof(type2));\
      printf(#funcName","#lib","#operation","#float_precision","#vectorInstruction","#VerctorWidth","#ulp","#start"~"#end",%g\n",(double)(t)/(niter*count));\
      fflush(stdout); \
    })

#define callFuncSleefToPoint1_1(funcName, arg, type,lib,operation,float_precision,vectorInstruction,VerctorWidth,ulp,point,type2,...) ({ \
      fillByOne(arg,niter*veclen,point,random_sed++,type2);         \
      uint64_t t = Sleef_currentTimeMicros();int i=0;int j=0;           \
      for(j=0;j<niter2;j++) {                                   \
        type *p = (type *)(arg);                                        \
        for(i=0;i<niter1;i++) funcName(*p++);                   \
      }                                                                 \
      t=Sleef_currentTimeMicros() - t;\
      int count = (sizeof(type)/sizeof(type2));\
      printf(#funcName","#lib","#operation","#float_precision","#vectorInstruction","#VerctorWidth","#ulp","#point",%g\n",(double)(t)/(niter*count));\
      fflush(stdout); \
    })

#define callFuncSleef2_1(funcName, arg, type,lib,operation,float_precision,vectorInstruction,VerctorWidth,ulp,start,end,type2,...) ({				\
      fillByTwo(arg,niter*veclen,start,end,random_sed++,type2,__VA_ARGS__);					\
      uint64_t t = Sleef_currentTimeMicros();int i=0;int j=0;		\
      for(j=0;j<niter2;j++) {					\
        type *p = (type *)(arg), c;					\
        for(i=0;i<niter1;i++) funcName(&c, *p++);			\
      }									\
      t=Sleef_currentTimeMicros() - t;\
      int count = (sizeof(type)/sizeof(type2));\
      printf(#funcName","#lib","#operation","#float_precision","#vectorInstruction","#VerctorWidth","#ulp","#start"~"#end",%g\n",(double)(t)/(niter*count));\
      fflush(stdout);							\
    })

#define callFuncSleefToPoint2_1(funcName, arg, type,lib,operation,float_precision,vectorInstruction,VerctorWidth,ulp,point,type2,...) ({        \
      fillByOne(arg,niter*veclen,point,random_sed++,type2);         \
      uint64_t t = Sleef_currentTimeMicros();int i=0;int j=0;   \
      for(j=0;j<niter2;j++) {         \
        type *p = (type *)(arg), c;         \
        for(i=0;i<niter1;i++) funcName(&c, *p++);     \
      }                 \
      t=Sleef_currentTimeMicros() - t;\
      int count = (sizeof(type)/sizeof(type2));\
      printf(#funcName","#lib","#operation","#float_precision","#vectorInstruction","#VerctorWidth","#ulp","#point",%g\n",(double)(t)/(niter*count));\
      fflush(stdout);\
    })

#define callFuncSleef1_2(funcName, arg1, arg2, type,lib,operation,float_precision,vectorInstruction,VerctorWidth,ulp,start1,end1,start2,end2,type2,...) ({			\
      fillByTwo(arg1,niter*veclen,start1,end1,random_sed++,type2,__VA_ARGS__);fillByTwo(arg2,niter*veclen,start2,end2,random_sed++,type2,__VA_ARGS__);				\
      uint64_t t = Sleef_currentTimeMicros();int i=0;int j=0;				\
      for(j=0;j<niter2;j++) {					\
        type *p1 = (type *)(arg1), *p2 = (type *)(arg2);		\
        for(i=0;i<niter1;i++) funcName(*p1++, *p2++);		\
      }									\
      t=Sleef_currentTimeMicros() - t;\
      int count = (sizeof(type)/sizeof(type2));\
      printf(#funcName","#lib","#operation","#float_precision","#vectorInstruction","#VerctorWidth","#ulp","#start1"~"#end1"&"#start2"~"#end2",%g\n",(double)(t)/(niter*count));\
      fflush(stdout);							\
    })

#define callFuncSleefToPoint1_2(funcName, arg1, arg2, type,lib,operation,float_precision,vectorInstruction,VerctorWidth,ulp,point1,point2,type2,...) ({ \
      fillByOne(arg1,niter*veclen,point1,random_sed++,type2);fillByOne(arg2,niter*veclen,point2,random_sed++,type2);\
      uint64_t t = Sleef_currentTimeMicros();int i=0;int j=0;       \
      for(j=0;j<niter2;j++) {         \
        type *p1 = (type *)(arg1), *p2 = (type *)(arg2);    \
        for(i=0;i<niter1;i++) funcName(*p1++, *p2++);   \
      }                 \
      t=Sleef_currentTimeMicros() - t;\
      int count = (sizeof(type)/sizeof(type2));\
      printf(#funcName","#lib","#operation","#float_precision","#vectorInstruction","#VerctorWidth","#ulp","#point1","#point2",%g\n",(double)(t)/(niter*count));\
      fflush(stdout);             \
    })

#define callFuncSvml1_1(funcName, arg, type,lib,operation,float_precision,vectorInstruction,VerctorWidth,start,end,type2,...) ({				\
      fillByTwo(arg,niter*veclen,start,end,random_sed++,type2,__VA_ARGS__);				\
      uint64_t t = Sleef_currentTimeMicros();int i=0;int j=0;				\
      for(j=0;j<niter2;j++) {					\
        type *p = (type *)(arg);					\
        for(i=0;i<niter1;i++) funcName(*p++);			\
      }									\
      t=Sleef_currentTimeMicros() - t;\
      int count = (sizeof(type)/sizeof(type2));\
      printf(#funcName","#lib","#operation","#float_precision","#vectorInstruction","#VerctorWidth","SVMLULP #start"~"#end",%g\n",(double)(t)/(niter*count));\
      fflush(stdout);							\
    })

#define callFuncSvmlToPoint1_1(funcName, arg, type,lib,operation,float_precision,vectorInstruction,VerctorWidth,point,type2,...) ({\
      fillByOne(arg,niter*veclen,point,random_sed++,type2);\
      uint64_t t = Sleef_currentTimeMicros();int i=0;int j=0;\
      for(j=0;j<niter2;j++) {\
        type *p = (type *)(arg);\
        for(i=0;i<niter1;i++) funcName(*p++);\
      }\
      t=Sleef_currentTimeMicros() - t;\
      int count = (sizeof(type)/sizeof(type2));\
      printf(#funcName","#lib","#operation","#float_precision","#vectorInstruction","#VerctorWidth","SVMLULP #point",%g\n",(double)(t)/(niter*count));\
      fflush(stdout);\
    })

#define callFuncSvml2_1(funcName, arg, type,lib,operation,float_precision,vectorInstruction,VerctorWidth,start,end,type2,...) ({\
      fillByTwo(arg,niter*veclen,start,end,random_sed++,type2,__VA_ARGS__);					\
      uint64_t t = Sleef_currentTimeMicros();int i=0;int j=0;				\
      for(j=0;j<niter2;j++) {					\
        type *p = (type *)(arg), c;					\
        for(i=0;i<niter1;i++) funcName(&c, *p++);			\
      }									\
      t=Sleef_currentTimeMicros() - t;\
      int count = (sizeof(type)/sizeof(type2));\
      printf(#funcName","#lib","#operation","#float_precision","#vectorInstruction","#VerctorWidth","SVMLULP #start"~"#end",%g\n",(double)(t)/(niter*count));\
      fflush(stdout);							\
    })

#define callFuncSvmlToPoint2_1(funcName, arg, type,lib,operation,float_precision,vectorInstruction,VerctorWidth,point,type2,...) ({        \
      fillByOne(arg,niter*veclen,point,random_sed++,type2);         \
      uint64_t t = Sleef_currentTimeMicros();int i=0;int j=0;       \
      for(j=0;j<niter2;j++) {         \
        type *p = (type *)(arg), c;         \
        for(i=0;i<niter1;i++) funcName(&c, *p++);     \
      }                 \
      t=Sleef_currentTimeMicros() - t;\
      int count = (sizeof(type)/sizeof(type2));\
      printf(#funcName","#lib","#operation","#float_precision","#vectorInstruction","#VerctorWidth","SVMLULP #point",%g\n",(double)(t)/(niter*count));\
      fflush(stdout);             \
    })

#define callFuncSvml1_2(funcName, arg1, arg2, type,lib,operation,float_precision,vectorInstruction,VerctorWidth,start1,end1,start2,end2,type2,...) ({\
      fillByTwo(arg1,niter*veclen,start1,end1,random_sed++,type2,__VA_ARGS__);fillByTwo(arg2,niter*veclen,start2,end2,random_sed++,type2,__VA_ARGS__);\
      uint64_t t = Sleef_currentTimeMicros();int i=0;int j=0;				\
      for(j=0;j<niter2;j++) {					\
        type *p1 = (type *)(arg1), *p2 = (type *)(arg2);		\
        for(i=0;i<niter1;i++) funcName(*p1++, *p2++);		\
      }	\
      t=Sleef_currentTimeMicros() - t;\
      int count = (sizeof(type)/sizeof(type2));\
      printf(#funcName","#lib","#operation","#float_precision","#vectorInstruction","#VerctorWidth","SVMLULP #start1"~"#end1"&"#start2"~"#end2",%g\n",(double)(t)/(niter*count));\
      fflush(stdout);\
    })

#define callFuncSvmlToPoint1_2(funcName, arg1, arg2, type,lib,operation,float_precision,vectorInstruction,VerctorWidth,point1,point2,type2,...) ({\
      fillByOne(arg1,niter*veclen,point1,random_sed++,type2);fillByOne(arg2,niter*veclen,point2,random_sed++,type2);\
      uint64_t t = Sleef_currentTimeMicros();int i=0;int j=0;       \
      for(j=0;j<niter2;j++) {         \
        type *p1 = (type *)(arg1), *p2 = (type *)(arg2);    \
        for(i=0;i<niter1;i++) funcName(*p1++, *p2++);   \
      }                 \
      t=Sleef_currentTimeMicros() - t;\
      int count = (sizeof(type)/sizeof(type2));\
      printf(#funcName","#lib","#operation","#float_precision","#vectorInstruction","#VerctorWidth","SVMLULP #point1 "& "#point2",%g\n",(double)(t)/(niter*count));\
      fflush(stdout);\
    })

#define callFuncInit() \
  double *abufDP = Sleef_malloc(sizeof(double) * niter * veclen);\
  double *bbufDP = Sleef_malloc(sizeof(double) * niter * veclen);\
  float  *abufSP = Sleef_malloc(sizeof(float)  * niter * veclen);\
  float  *bbufSP = Sleef_malloc(sizeof(float)  * niter * veclen);\
  double a,b,acc = 0;\
  int random_sed = 0;\

#define callFuncEnd() \
  free(abufSP);\
  free(abufDP);\
  free(bbufSP);\
  free(bbufDP);\
  printf("dummy value : %g\n", acc);\


static  int niter1 = 10000, niter2 = 10000;
static  int niter = 100000000; //?
static  int veclen = MY_VECLEN;
static void callFuncSetInitNiter(int i){
  niter = i;  
}
static void callFuncSetVeclen(int new_veclen){
    veclen = new_veclen;
}
