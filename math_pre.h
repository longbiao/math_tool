#ifndef __MATH_PRE_H_
#define __MATH_PRE_H_

#include "fill.h"
#include "ulp.h"

#if (defined(__GNUC__) || defined(__CLANG__)) && (defined(__i386__) || defined(__x86_64__))
#include <x86intrin.h>
#endif
#if (defined(_MSC_VER))
#include <intrin.h>
#endif
#if defined(__ARM_NEON__) || defined(__ARM_NEON)
#include <arm_neon.h>
#endif

#define callFuncPre(funcName,funcNameMpfr,funcNameSTD,type,type2,start,end,step) ({ \
    double e,maxUlp=0.0,avgUlp=0.0;int count=0;\
    type2 a;\
    type2 *abufDP;       \
    posix_memalign((void **)&abufDP, 16*sizeof(type2), 16*sizeof(type2)); \
    for(double index = HANDLE_BOUNDARY(start,step);index <= end;index += step) {            \
        a = (type2)return_signed(count) * (type2)index;\
        mpfr_set_d(frt, a, GMP_RNDN);                    \
        funcNameMpfr(frt, frt, GMP_RNDN);                \
        for(int i = 0;i < 16;i++){ abufDP[i] = a;}                   \
        type *p =(type *)abufDP;                                   \
        type result = funcName(*p);                        \
        type2 *b=(type2 *)(& result);                          \
        e = countULP_##type2(*b,frt);\
        if(e > epsion){                     \
            fprintf(fp,"####\n");\
            fprintf(fp,#funcName ",ulp:%g ,operation:%10.128f \n",e,a); \
            fprintf(fp,"result     : %10.128f\n",*b);                   \
            mpfr_fprintf(fp,"result_mpfr: %10.128Rf\n",frt);                \
            type resultz = funcNameSTD(*p);\
            type2 *bz = (type2 *)(& resultz);                   \
            fprintf(fp,"result_std : %10.128f\n",*bz);                     \
        }\
        maxUlp = fmax(maxUlp,e);avgUlp += e;count++;\
    }\
    free(abufDP);\
    printf(#funcName ",count : %d,start:"#start ",end:" #end",step:" #step ",maxUlp:%g ,avgUlp:%g \n",count,maxUlp,avgUlp/count);\
    fflush(stdout);\
})

#define callFuncPre2_1(funcName,funcNameMpfr,funcNameSTD,type,type2,start1,end1,step1,start2,end2,step2) ({ \
    double e,maxUlp=0.0,avgUlp=0.0;int count=0;\
    type2 a,c;\
    type2 *abufDP ; type2 *bbufDP;\
    posix_memalign((void **)&abufDP, 16*sizeof(type2), 16*sizeof(type2));\
    posix_memalign((void **)&bbufDP, 16*sizeof(type2), 16*sizeof(type2));\
    for(double index0 = HANDLE_BOUNDARY(start1,step1);index0 <= end1;index0 += step1) {            \
        a = (type2)index0 * (type2)return_signed(count);\
        for(double index1 = HANDLE_BOUNDARY(start2,step2) ; index1 <= end2 ; index1 += step2 ){    \
            c = (type2)index1 * (type2)return_signed(count);\
            mpfr_set_d(frt, (type2)a, GMP_RNDN);mpfr_set_d(frt2,(type2)c,GMP_RNDN);                    \
            funcNameMpfr(frt, frt,frt2, GMP_RNDN);                \
            for(int i = 0; i < 16;i++){ abufDP[i] = (type2)a ; bbufDP[i] = (type2)c;}                   \
            type *p =(type *)abufDP; type *p2 = (type *)bbufDP;                                  \
            type result = funcName(*p,*p2);                        \
            type2 *b=(type2 *)(& result)  ;                          \
            e = countULP_##type2(*b,frt);\
            if(e > epsion){                                         \
                fprintf(fp,"####\n");\
                fprintf(fp,#funcName ",ulp:%g ,operation:%g %g\n",e,a,c); \
                fprintf(fp,"result     : %.128f\n",*b);                                   \
                mpfr_fprintf(fp,"result_mpfr: %.128Rf\n",frt);                             \
                type resultz = funcNameSTD(*p,*p2);                 \
                type2 *bz = (type2 *)(& resultz);\
                fprintf(fp,"result_std : %.128f\n",*bz);\
            }\
            maxUlp = fmax(maxUlp,e);\
            avgUlp += e;\
            count++;\
        }\
    }\
    free(abufDP);free(bbufDP);\
    printf(#funcName ",count : %d,start1:"#start1 ",end1:" #end1 ",step1:" #step1 ",start2:"#start2 ",end2:" #end2 ",step2:" #step2 ",maxUlp:%g ,avgUlp:%g \n",count,maxUlp,avgUlp/count);                            \
    fflush(stdout);\
})

#define callFuncPreF(funcName,funcNameMpfr,funcNameSTD,type,type2,start,end,step) ({ \
    double e,maxUlp=0.0,avgUlp=0.0;int count=0;\
    type2 *abufDP;\
    posix_memalign((void **)&abufDP, 16*sizeof(type2), 16*sizeof(type2));\
    for(double a = HANDLE_BOUNDARY(start,step);a <= end;a += step) {\
        fill_##type2(abufDP,16,start,end,count);\
        abufDP[0]=(type2)a;                   \
        type *p =(type *)abufDP;                                   \
        type result = funcName(*p);                        \
        type2 *b=(type2 *)(& result)  ;                          \
        for(int i = 0 ; i < (sizeof(type)/sizeof(type2)) ;i++ ) \
        {                              \
            type2 bone = b[i];                        \
            /*fprintf(fp,"%d : %10.1280f ",i,abufDP[i]);    */        \
            mpfr_set_d(frt,abufDP[i],GMP_RNDN);                               \
            funcNameMpfr(frt, frt, GMP_RNDN);               \
            e=countULP_##type2(bone,frt);\
            if(e > epsion){                     \
                fprintf(fp,"####\n");\
                fprintf(fp,#funcName ",ulp:%g ,operation:%10.128f \n",e,abufDP[i]); \
                fprintf(fp,"result     : %10.128f\n",bone);                 \
                mpfr_fprintf(fp,"result_mpfr: %10.128Rf\n",frt);                \
                type resultz = funcNameSTD(*p);\
                type2 *bz = (type2 *)(& resultz);                   \
                fprintf(fp,"result_std : %10.128f\n",bz[i]);                     \
            }                                       \
            maxUlp = fmax(maxUlp,e);                                \
            avgUlp += e;                                            \
            count++;                                                \
        }                           \
    }                                                               \
    free(abufDP);\
    printf(#funcName ",count : %d,start:"#start ",end:" #end",step:" #step ",maxUlp:%g ,avgUlp:%g \n",count,maxUlp,avgUlp/count); \
    fflush(stdout);\
})

#define callFuncPreF2_1(funcName,funcNameMpfr,funcNameSTD,type,type2,start1,end1,step1,start2,end2,step2) ({ \
    double e,maxUlp=0.0,avgUlp=0.0;int count=0;\
    type2 *abufDP ; type2 *bbufDP ;           \
    posix_memalign((void **)&abufDP, 16*sizeof(type2), 16*sizeof(type2)); \
    posix_memalign((void **)&bbufDP, 16*sizeof(type2), 16*sizeof(type2)); \
    for(double a = HANDLE_BOUNDARY(start1,step1);a <= end1;a += step1) {            \
        for(double c = HANDLE_BOUNDARY(start2,step2);c <= end2;c += step2 ){\
            fill_##type2(abufDP,16,start1,end1,count);fill_##type2(bbufDP,16,start2,end2,count);\
            abufDP[0] = (type2)a;bbufDP[0] = (type2)c;\
            type *p =(type *)abufDP; type *p2 = (type *)bbufDP;                                  \
            type result = funcName(*p,*p2);                        \
            type2 *b=(type2 *)(& result)  ;                          \
            for(int i = 0 ; i < (sizeof(type)/sizeof(type2)) ;i++ ) \
            {                               \
                type2 bone = b[i];\
                mpfr_set_d(frt, abufDP[i], GMP_RNDN);\
                mpfr_set_d(frt2,bbufDP[i],GMP_RNDN);\
                funcNameMpfr(frt, frt,frt2, GMP_RNDN);\
                e=countULP_##type2(bone,frt);\
                if(e > epsion){                                         \
                    fprintf(fp,"####\n");\
                    fprintf(fp,#funcName ",ulp:%g ,operation:%g %g\n",e,abufDP[i],bbufDP[i]); \
                    fprintf(fp,"result     : %.128f\n",bone);                                   \
                    mpfr_fprintf(fp,"result_mpfr: %.128Rf\n",frt);                             \
                    type resultz = funcNameSTD(*p,*p2);                 \
                    type2 *bz = (type2 *)(& resultz);           \
                    fprintf(fp,"result_std : %.128f\n",bz[i]);                           \
                }                                        \
                maxUlp = fmax(maxUlp,e)     ;                                                \
                avgUlp += e;                                                                 \
                count++ ;                                                                    \
            }\
        }\
    }\
    free(abufDP);free(bbufDP);\
    printf(#funcName ",count : %d,start1:"#start1 ",end1:" #end1 ",step1:" #step1 ",start2:"#start2 ",end2:" #end2 ",step2:" #step2 ",maxUlp:%g ,avgUlp:%g \n",count,maxUlp,avgUlp/count);                            \
    fflush(stdout);\
})

#define callFuncPreFT(funcName,funcNameMpfr,funcNameSTD,type,type2,start,end,step) ({ \
  double e,maxUlp=0.0,avgUlp=0.0;int count=0;\
  type2 *abufDP;       \
  posix_memalign((void **)&abufDP, 16*sizeof(type2), 16*sizeof(type2));\
  for(double a = (start+step);a <= end;a += step) {\
    fill_toward_boundary_##type2(abufDP,16,start,end,count);\
    type *p =(type *)abufDP;                                   \
    type result = funcName(*p);                        \
    type2 *b=(type2 *)(& result)  ;                          \
    for(int i = 0 ; i < (sizeof(type)/sizeof(type2)) ;i++ ) \
    {                \
      type2 bone = b[i];              \
      /*printf("%d : %0.128f \n",i,abufDP[i]);*/        \
      mpfr_set_d(frt,abufDP[i],GMP_RNDN);                               \
      funcNameMpfr(frt, frt, GMP_RNDN);       \
      e=countULP_##type2(bone,frt);\
      if(e > epsion){           \
       fprintf(fp,"####\n");\
       fprintf(fp,#funcName ",ulp:%g ,operation:%10.128f \n",e,abufDP[i]); \
       fprintf(fp,"result     : %10.128f\n",bone);          \
       mpfr_fprintf(fp,"result_mpfr: %10.128Rf\n",frt);       \
       type resultz = funcNameSTD(*p);\
       type2 *bz = (type2 *)(& resultz);          \
       fprintf(fp,"result_std : %10.128f\n",bz[i]);                     \
      }\
      maxUlp = fmax(maxUlp,e);avgUlp += e;count++;\
    }\
  }\
  free(abufDP);\
  printf(#funcName ",count : %d,start:"#start ",end:" #end",step:" #step ",maxUlp:%g ,avgUlp:%g \n",count,maxUlp,avgUlp/count); \
  fflush(stdout);\
})

#define callFuncPreFT2_1(funcName,funcNameMpfr,funcNameSTD,type,type2,start1,end1,step1,start2,end2,step2) ({\
  double e,maxUlp=0.0,avgUlp=0.0;int count=0;\
  type2 *abufDP ; type2 *bbufDP ;\
  posix_memalign((void **)&abufDP, 16*sizeof(type2), 16*sizeof(type2)); \
  posix_memalign((void **)&bbufDP, 16*sizeof(type2), 16*sizeof(type2)); \
  for(double a = (start1+step1);a <= end1;a += step1) {            \
    for(double c = (start2+step2);c <= end2;c += step2 ){\
        fill_toward_boundary_##type2(abufDP,16,start1,end1,count);fill_toward_boundary_##type2(bbufDP,16,start2,end2,count);\
        type *p =(type *)abufDP; type *p2 = (type *)bbufDP;                                  \
        type result = funcName(*p,*p2);                        \
        type2 *b=(type2 *)(& result)  ;                          \
        for(int i = 0 ; i < (sizeof(type)/sizeof(type2)) ;i++ ) \
        {               \
          type2 bone = b[i];\
          mpfr_set_d(frt, abufDP[i], GMP_RNDN);mpfr_set_d(frt2,bbufDP[i],GMP_RNDN);                    \
          funcNameMpfr(frt, frt,frt2, GMP_RNDN);                \
          e=countULP_##type2(bone,frt);\
          if(e > epsion){                                         \
            fprintf(fp,"####\n");\
            fprintf(fp,#funcName ",ulp:%g ,operation:%g %g\n",e,abufDP[i],bbufDP[i]); \
            fprintf(fp,"result     : %.128f\n",bone);                                   \
            mpfr_fprintf(fp,"result_mpfr: %.128Rf\n",frt);                             \
            type resultz = funcNameSTD(*p,*p2);         \
            type2 *bz = (type2 *)(& resultz);     \
            fprintf(fp,"result_std : %.128f\n",bz[i]);                           \
          }                      \
          maxUlp = fmax(maxUlp,e);avgUlp += e;count++ ;\
      }\
    } \
  }\
  free(abufDP);free(bbufDP);\
  printf(#funcName ",count : %d,start1:"#start1 ",end1:" #end1 ",step1:" #step1 ",start2:"#start2 ",end2:" #end2 ",step2:" #step2 ",maxUlp:%g ,avgUlp:%g \n",count,maxUlp,avgUlp/count);                            \
  fflush(stdout);\
})

#define callFuncPre_diff_range1(funcName,funcNameMpfr,funcNameSTD,type,type2,niter,range_array,range_array_length) ({ \
    double e,maxUlp=0.0,avgUlp=0.0;int count=0;\
    int    i,j,index,count;\
    int random_sed = time(NULL);\
    type2 *array;\
    int array_length=sizeof(type)/sizeof(type2);\
    posix_memalign((void **)&array,16*sizeof(type2),16*sizeof(type2));\
    count = 0;\
    for(i = 0; i < niter;i++){\
        fill_random_##type2(array,array_length,range_array,range_array_length,random_sed+i);\
        type *p = (type *)array;\
        type  func_result = funcName(*p);\
        type2 *func_result_sca = (type2 *)(& func_result);\
        for(j = 0;j<array_length;j++){\
          type2 result_one = func_result_sca[j];\
          mpfr_set_d(frt,array[j],GMP_RNDN);\
          funcNameMpfr(frt,frt,GMP_RNDN);\
          e=countULP_##type2(result_one,frt);\
          if(e > epsion){\
            fprintf(fp,"####\n");\
            fprintf(fp,#funcName ",ulp:%g ,operation:%10.128f \n",e,array[j]);\
            fprintf(fp,"result     : %10.128f\n",result_one);\
            mpfr_fprintf(fp,"result_mpfr: %.128Rf\n",frt);\
            type resultz = funcNameSTD(*p);\
            type2 *bz = (type2 *)(& resultz);\
            fprintf(fp,"result_std : %10.128f\n",bz[j]);\
          }\
          maxUlp = fmax(maxUlp,e);\
          avgUlp += e;\
          count ++;\
        }\
        fprintf(fp,"\n");\
    }\
    free(abufDP);\
    printf(#funcName ",niter : %d ,maxUlp:%g ,avgUlp:%g \n",niter,maxUlp,avgUlp/count);\
    fflush(stdout);\
})

#define callFuncPre_diff_range(funcName,funcNameMpfr,funcNameSTD,type,type2,niter,...) ({ \
    double e,maxUlp=0.0,avgUlp=0.0;int count=0;\
    type2 range_array[] = {__VA_ARGS__};\
    long   range_array_length = sizeof(range_array)/sizeof(type2);\
    int    i,j,index;\
    int random_sed = time(NULL);\
    type2 *array;\
    int array_length=sizeof(type)/sizeof(type2);\
    posix_memalign((void **)&array,16*sizeof(type2),16*sizeof(type2));\
    count = 0;\
    for(i = 0; i < niter;i++){\
        fill_random_##type2(array,array_length,range_array,range_array_length,random_sed+i);\
        type *p = (type *)array;\
        type  func_result = funcName(*p);\
        type2 *func_result_sca = (type2 *)(& func_result);\
        for(j = 0;j<array_length;j++){\
          type2 result_one = func_result_sca[j];\
          mpfr_set_d(frt,array[j],GMP_RNDN);\
          funcNameMpfr(frt,frt,GMP_RNDN);\
          e=countULP_##type2(result_one,frt);\
          if(e > epsion){           \
            fprintf(fp,"####\n");\
            fprintf(fp,#funcName ",ulp:%g ,operation:%10.128f \n",e,array[j]);\
            fprintf(fp,"result     : %10.128f\n",result_one);\
            mpfr_fprintf(fp,"result_mpfr: %.128Rf\n",frt);\
            type resultz = funcNameSTD(*p);\
            type2 *bz = (type2 *)(& resultz);\
            fprintf(fp,"result_std : %10.128f\n",bz[j]);\
          }                     \
          maxUlp = fmax(maxUlp,e);\
          avgUlp += e;\
          count ++; \
        }\
    }                     \
    free(array);\
    printf(#funcName ",niter : %d,maxUlp:%g ,avgUlp:%g \n",niter,maxUlp,avgUlp/count);\
    fflush(stdout);\
})


#define callFuncPre_point1_1(funcName,funcNameMpfr,funcNameSTD,type,type2,point) ({\
  double e,maxUlp=0.0,avgUlp=0.0;int count=0;\
  type2 arg1 = point;\
  double ulp = 0,e = 0;\
  type2 *abufDP;\
  mpfr_set_d(frt, arg1, GMP_RNDN);\
  funcNameMpfr(frt, frt, GMP_RNDN);\
  posix_memalign((void **)&abufDP, 16*sizeof(type2), 16*sizeof(type2));\
  for(int i = 0;i < 16;i++){ abufDP[i] = arg1;}\
  type *p1 =(type *)abufDP;\
  type result1_1 = funcName(*p1);\
  type2 *result2_1 = (type2 *)(& result1_1);\
  e = countULP_##type2(*result2_1,frt);\
  if(e >= epsion){\
    fprintf(fp,"####\n");\
    fprintf(fp,#funcName ",ulp:%g ,operation:%10.128f \n",e,abufDP[0]);\
    fprintf(fp,"result     : %10.128f\n",*result2_1);\
    mpfr_fprintf(fp,"result_mpfr: %10.128Rf\n",frt);\
    type result3_1 = funcNameSTD(*p1);\
    type2 *result4_1 = (type2 *)(& result3_1);\
    fprintf(fp,"result_std : %10.128f\n",*result4_1);\
  }\
  free(abufDP);\
  printf(#funcName ",point : %lf, ulp : %f\n",point,e);\
  fflush(stdout);\
})

#define calFuncToPointS1_1(funcName,funcNameMpfr,funcNameSTD,type,type2,type3,args1,ni) ({\
  double e,maxUlp=0.0,avgUlp=0.0;int count=0;\
  type2 tmp,arg1 = args1;\
  int i = 0,j = 0,niter = ni,flag;\
  int random_sed = time(NULL);\
  srandom(random_sed);\
  type2 *array;\
  uint64_t random_value;\
  posix_memalign((void **)&array,16*sizeof(type2),16*sizeof(type2));\
  for(i = 0;i<niter;i++){\
    random_value = random();\
    flag = (int)(((int32_t)(((int32_t)random_value & 0x1) << 31) >> 31) ^ 0x1);\
    tmp = rnd_someone_double(arg1,flag);\
    for(j = 0;j < 16;j++){array[j]=tmp;}\
    type *p1 = (type *)array;\
    type result1 = funcName(*p1);\
    type2 *result2 = (type2 *)(& result1);\
    mpfr_set_d(frt,array[0],GMP_RNDN);\
    funcNameMpfr(frt, frt, GMP_RNDN);\
    e=countULP_##type2(*result2,frt);\
    if(e >= epsion){\
      fprintf(fp,"####\n");\
      fprintf(fp,#funcName ",ulp:%g ,operation:%10.1280f \n",e,array[0]);\
      fprintf(fp,"result     : %10.128f\n",*result2);\
      mpfr_fprintf(fp,"result_mpfr: %10.128Rf\n",frt);\
      type result3 = funcNameSTD(*p1);\
      type2 *result4 = (type2 *)(& result3);\
      fprintf(fp,"result_std : %10.128f\n",*result4);\
    }\
    maxUlp = fmax(maxUlp,e);avgUlp += e;count ++; \
  }\
  free(array);\
  printf(#funcName ",niter : %d,maxUlp:%g ,avgUlp:%g \n",niter,maxUlp,avgUlp/count);\
})

#define calFuncToPointF1_1(funcName,funcNameMpfr,funcNameSTD,type,type2,type3,args1,ni) ({\
  double e,maxUlp=0.0,avgUlp=0.0;int count=0;\
  type2 tmp,arg1 = args1;\
  int i = 0,j = 0,niter = ni;\
  int random_sed = time(NULL);\
  type2 *array;\
  posix_memalign((void **)&array,16*sizeof(type2),16*sizeof(type2));\
  for(i = 0 ;i<niter;i++){\
    fill_toward_point_##type2(array,16,arg1,random_sed+i);\
    type *p1 = (type *)array;\
    type result1 = funcName(*p1);\
    type2 *result2 = (type2 *)(& result1);\
    for(j=0;j<(sizeof(type)/sizeof(type2));j++){\
        mpfr_set_d(frt,array[j],GMP_RNDN);\
        funcNameMpfr(frt,frt,GMP_RNDN);\
        e=countULP_##type2(result2[j],frt);\
        if(e >= epsion){\
          fprintf(fp,"####\n");\
          fprintf(fp,#funcName ",ulp:%g ,operation:%10.128f \n",e,array[j]);\
          fprintf(fp,"result     : %10.128f\n",result2[j]);\
          mpfr_fprintf(fp,"result_mpfr: %10.128Rf\n",frt);\
          type result3 = funcNameSTD(*p1);\
          type2 *result4 = (type2 *)(& result3);\
          fprintf(fp,"result_std : %10.128f\n",result4[j]);\
        }\
        maxUlp = fmax(maxUlp,e);avgUlp += e;count ++; \
    }\
  }\
  free(array);\
  printf(#funcName ",niter : %d,maxUlp:%g ,avgUlp:%g \n",niter,maxUlp,avgUlp/count);\
})

#define callFuncPreMixAll1_1(funcName,funcNameMpfr,funcNameSTD,type1,type2,type3,cniter,...) ({\
  double e,maxUlp=0.0,avgUlp=0.0;int count=0;\
  int i,j,k,index,niter=cniter;\
  type2 targetArray[]={__VA_ARGS__,POSITIVE_INFINITY_##type2,NEGATIVE_INFINITY_##type2,-0.0,+0.0,-DENORMAL_MIN_##type2,+DENORMAL_MIN_##type2};\
  /*add +-nan ,+-inf,+-denormal_min*/\
  int targetArray_length = sizeof(targetArray)/sizeof(type2);\
  int vec_wid = sizeof(type1)/sizeof(type2);\
  type2 *abuf;\
  posix_memalign((void **)&abuf,16*sizeof(type2),vec_wid*sizeof(type2));\
  for(i=0;i<niter;i++){\
    fill_random_bymix_##type2(targetArray,targetArray_length,abuf,vec_wid,i);\
    type1 *p1 = (type1 *)abuf;\
    type1 result1 = funcName(*p1);\
    type2 *result2 = (type2 *)(& result1);\
    for(j = 0;j < vec_wid;j++){\
      mpfr_set_d(frt,abuf[j],GMP_RNDN);\
      funcNameMpfr(frt,frt,GMP_RNDN);\
      e=countULP_##type2(result2[j],frt);\
      if(e >= epsion){\
        fprintf(fp,"####\n");\
        fprintf(fp,#funcName ",ulp:%g ,operation:%10.128f \n",e,abuf[j]);\
        fprintf(fp,"result     : %10.128f\n",result2[j]);\
        mpfr_fprintf(fp,"result_mpfr: %10.128Rf\n",frt);\
        type1 result3 = funcNameSTD(*p1);\
        type2 *result4 = (type2 *)(& result3);\
        fprintf(fp,"result_std : %10.128f\n",result4[j]);\
      }\
      maxUlp = fmax(maxUlp,e);avgUlp += e;count ++; \
    }\
  }\
  free(abuf);\
  printf(#funcName ",niter : %d,maxUlp:%g ,avgUlp:%g \n",niter,maxUlp,avgUlp/count);\
})\
  // delete the +-inf +-0 +-denor in array
  // add the +-inf +-0 +-denor to array
  // sort the array
  // add the -nan to array[0],add the +nan to array[1];
  // if the array[i] is nan,and only make the point to be value;

#define callFuncSomeSpecialValue1_1(test_funcName,mpfr_funcName,std_funcName,test_func_type,std_func_type,scalar_type,SpecialValueArray) ({\
    double e,maxUlp=0.0,avgUlp=0.0;int count=0;\
    int64_t length = sizeof(SpecialValueArray)/sizeof(scalar_type);\
    int64_t i;\
    int tmp;\
    scalar_type a;\
    scalar_type *abuf;\
    posix_memalign((void **)&abuf, 16*sizeof(scalar_type), 16*sizeof(scalar_type));\
    for(i = 0;i < length;i++){\
      a = SpecialValueArray[i];\
      mpfr_set_d(frt, a, GMP_RNDN);\
      mpfr_funcName(frt,frt,GMP_RNDN);\
      for(tmp = 0;tmp < 16; tmp++){ abuf[tmp] = a;}\
      test_func_type *p = (test_func_type *)abuf;\
      test_func_type result = test_funcName(*p);\
      scalar_type *b =(scalar_type *)(& result);\
      e = countULP_##scalar_type(*b,frt);\
      if(e > epsion){        \
        fprintf(fp,"####\n");\
        fprintf(fp,#test_funcName ",ulp:%g ,operation:%10.128f \n",e,a); \
        fprintf(fp,"result     : %10.128f\n",*b);                   \
        mpfr_fprintf(fp,"result_mpfr: %10.128Rf\n",frt);            \
        std_func_type result_std = std_funcName(*p);                 \
        scalar_type *result_array = (scalar_type *)(& result_std);  \
        fprintf(fp,"result_std : %10.128f\n",*result_array);        \
      }\
    maxUlp = fmax(maxUlp,e);avgUlp += e;count++;\
    }\
    free(abuf);\
    printf(#test_funcName ",count : %d,maxUlp:%g ,avgUlp:%g \n",count,maxUlp,avgUlp/count);\
    fflush(stdout);\
})

#define callFuncSomeSpecialValue1_2(test_funcName,mpfr_funcName,std_funcName,test_func_type,std_func_type,scalar_type,SpecialValueArray) ({\
    double e,maxUlp=0.0,avgUlp=0.0;int count=0;\
    int64_t length = sizeof(SpecialValueArray)/sizeof(scalar_type);\
    int64_t i;\
    int tmp;\
    scalar_type opta,optb;\
    scalar_type *abuf,*bbuf;\
    posix_memalign((void **)&abuf, 16*sizeof(scalar_type), 16*sizeof(scalar_type));\
    posix_memalign((void **)&bbuf, 16*sizeof(scalar_type), 16*sizeof(scalar_type));\
    for(i = 0 ;i < length;i=i+2){\
      opta = SpecialValueArray[i];optb = SpecialValueArray[i+1];\
      mpfr_set_d(frt,opta,GMP_RNDN);mpfr_set_d(frt2,optb,GMP_RNDN);\
      mpfr_funcName(frt,frt,frt2,GMP_RNDN);\
      for(tmp = 0;tmp < 16; tmp++){ abuf[tmp] = opta;}\
      for(tmp = 0;tmp < 16; tmp++){ bbuf[tmp] = optb;}\
      test_func_type *opt_val_a = (test_func_type *)abuf;test_func_type *opt_val_b = (test_func_type *)bbuf;\
      test_func_type testFuncResult = test_funcName(*opt_val_a,*opt_val_b);\
      scalar_type *testFuncResultArray = (scalar_type *)(& testFuncResult);\
      e = countULP_##scalar_type(*testFuncResultArray,frt);\
      if(e >= epsion){\
        fprintf(fp,"####\n");\
        fprintf(fp,#test_funcName ",ulp:%g ,operation1:%10.128f,operation2:%10.128f \n",e,opta,optb); \
        fprintf(fp,"result     : %10.128f\n",*testFuncResultArray); \
        mpfr_fprintf(fp,"result_mpfr: %10.128Rf\n",frt);            \
        std_func_type resultStd = std_funcName(*opt_val_a,*opt_val_b);\
        scalar_type *resultStdArray = (scalar_type *)(& resultStd);  \
        fprintf(fp,"result_std : %10.128f\n",*resultStdArray);\
      }\
      maxUlp = fmax(maxUlp,e);avgUlp += e;count++;\
    }\
    free(abuf);free(bbuf);\
    printf(#test_funcName ",count : %d,maxUlp:%g ,avgUlp:%g \n",count,maxUlp,avgUlp/count);\
    fflush(stdout);\
})

//lineNum
#define callFuncSomeSpecialValueFromFile1_1(test_funcName,mpfr_funcName,std_funcName,test_func_type,std_func_type,scalar_type,specialValueFile,lineNum)({\
  double e,maxUlp=0.0,avgUlp=0.0;int count=0;\
  scalar_type *tmpArray,*abuf;\
  scalar_type opta;\
  int64_t tmpArrayLength = 20;\
  tmpArray = (scalar_type *)malloc(tmpArrayLength * sizeof(scalar_type));\
  posix_memalign((void **)&abuf, 16*sizeof(scalar_type), 16*sizeof(scalar_type));\
  int64_t fillLength = 0,usedChar = 0,offset = 0,stat;\
  int fd = open(specialValueFile,O_RDONLY);\
  int i,j;\
  while(count < lineNum * 2){\
    fillLength = fillSpecialValueFromFile_##scalar_type(fd,offset,tmpArray,tmpArrayLength,&stat);\
    if(stat == -1) {printf("error in read specail value from file [%s]\n",specialValueFile);exit(1);}\
    if(stat ==  0) {break;}\
    for(i = 0;i < fillLength; i++){\
        opta = tmpArray[i];\
        mpfr_set_d(frt,opta,GMP_RNDN);\
        mpfr_funcName(frt,frt,GMP_RNDN);\
        for( j = 0;j < 16 ;j++) {abuf[j] = opta;}\
        test_func_type *optVala = (test_func_type *)abuf;\
        test_func_type testFuncResult = test_funcName(*optVala);\
        scalar_type *testFuncResultArray = (scalar_type *)(& testFuncResult);\
        e = countULP_##scalar_type(*testFuncResultArray,frt);\
        if(e >= epsion){\
          fprintf(fp,"####\n");\
          fprintf(fp,#test_funcName ",ulp:%g ,operation1:%10.128f\n",e,opta);\
          fprintf(fp,"result     : %10.128f\n",*testFuncResultArray); \
          mpfr_fprintf(fp,"result_mpfr : %10.128Rf\n",frt);\
          std_func_type resultStd = std_funcName(*optVala);\
          scalar_type *resultStdArray = (scalar_type *)(& resultStd);\
          fprintf(fp,"result_std : %10.128f\n",*resultStdArray);\
        }\
        maxUlp = fmax(maxUlp,e);avgUlp += e;count++;\
    }\
    offset += stat;\
  }\
  free(tmpArray);free(abuf);\
  printf(#test_funcName ",count : %d,maxUlp:%g ,avgUlp:%g \n",count,maxUlp,avgUlp/count);\
  fflush(stdout);\
  close(fd);\
})

#define callFuncSomeSpecialValueFromFile1_2(test_funcName,mpfr_funcName,std_funcName,test_func_type,std_func_type,scalar_type,specialValueFile,lineNum)({\
  double e,maxUlp=0.0,avgUlp=0.0;int count=0;\
  scalar_type *tmpArray,*abuf,*bbuf;\
  scalar_type opta,optb;\
  int64_t tmpArrayLength = 20;\
  tmpArray = (scalar_type *)malloc(tmpArrayLength * sizeof(scalar_type));\
  posix_memalign((void **)&abuf,16*sizeof(scalar_type),16*sizeof(scalar_type));\
  posix_memalign((void **)&bbuf,16*sizeof(scalar_type),16*sizeof(scalar_type));\
  int64_t fillLength = 0,offset = 0,stat;\
  int fd = open(specialValueFile,O_RDONLY);\
  int i,j;\
  while(count < lineNum *2){\
    fillLength = fillSpecialValueFromFile_##scalar_type(fd,offset,tmpArray,tmpArrayLength,&stat);\
    if(stat == -1) {printf("error in read specail value from file [%s]\n",specialValueFile);exit(1);}\
    if(stat ==  0) {break;}\
    for(i = 0;(i+1) < fillLength;i+=2){\
      opta = tmpArray[i];optb = tmpArray[i+1];\
      mpfr_set_d(frt,opta,GMP_RNDN);mpfr_set_d(frt2,optb,GMP_RNDN);\
      mpfr_funcName(frt,frt,frt2,GMP_RNDN);\
      for(j = 0; j < 16;j++){abuf[j] = opta;}\
      for(j = 0; j < 16;j++){bbuf[j] = optb;}\
      test_func_type *optVala = (test_func_type *)abuf;\
      test_func_type *optValb = (test_func_type *)bbuf;\
      test_func_type testFuncResult = test_funcName(*optVala,*optValb);\
      scalar_type *testFuncResultArray = (scalar_type *)(& testFuncResult);\
      e = countULP_##scalar_type(*testFuncResultArray,frt);\
      if(e >= epsion){\
        fprintf(fp,"####\n");\
        fprintf(fp,#test_funcName ",ulp:%g,operation1 : %10.128f.operation2 : %10.128f\n",e,opta,optb);\
        fprintf(fp,"result     : %10.128f\n",*testFuncResultArray);\
        mpfr_fprintf(fp,"result_mpfr: %10.128Rf\n",frt);\
        std_func_type resultStd = std_funcName(*optVala,*optValb);\
        scalar_type *resultStdArray = (scalar_type *)(& resultStd);\
        fprintf(fp,"result_std : %10.128f\n",*resultStdArray);\
      }\
      maxUlp = fmax(maxUlp,e);avgUlp += e;count++;\
    }\
    offset += stat;\
  }\
  free(tmpArray);free(abuf);free(bbuf);\
  printf(#test_funcName ",count : %d,maxUlp:%g ,avgUlp:%g \n",count,maxUlp,avgUlp/count);\
  fflush(stdout);\
  close(fd);\
})

#define callFuncInit(myepsion,filename) \
  double epsion = myepsion;\
  mpfr_t frt, fru ,frt2;\
  mpfr_set_default_prec(128);\
  mpfr_inits(fra, frb, frc, frd, frt,frt2 ,fru, NULL);\
  FILE *fp;\
  if((fp = fopen(filename,"w"))==NULL)\
  {\
      printf("cant open errorUlp\n");\
      exit(50);\
  }\

#define callFuncEnd() \
  if(fclose(fp) != 0) printf("error close errorUlp \n");

#endif //(__MATH_PRE_H_)
