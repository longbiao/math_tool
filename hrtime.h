#ifndef __HRTIMR_H__
#define __HRTIMR_H__
#include <stdio.h>
#include <stdint.h>
#include "base.h"

#define M_PCM_CPUFREQ 2400 /* MHz */
typedef uint64_t pcm_hrtime_t;
#define NS2CYCLE(__ns) ((__ns) * M_PCM_CPUFREQ / 1000)
#define CYCLE2NS(__cycles) ((__cycles) * 1000 / M_PCM_CPUFREQ)

#if (defined(__GNUC__) || defined(__CLANG__)) && (defined(__i386__) || defined(__x86_64__))
    static INLINE unsigned long long asm_rdtsc (void)
    {
        unsigned hi, lo;
        __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
        return ((unsigned long long)lo) | (((unsigned long long)hi) << 32);
    }

#elif (defined(__GNUC__) || defined(__CLANG__)) && (defined(__arm__))
    static INLINE unsigned long read_counter()
    {
        unsigned long count;
        //asm ("mrs %0, cntpct_el0" : "=r"(count));
        asm("mrs %0, cntvct_el0" : "=r"(count));
        return count;
    }

    static INLINE unsigned long read_frq()
    {
        unsigned long frq;
        asm("mrs %0, cntfrq_el0" : "=r"(frq));
        return frq;
    }
    static INLINE unsigned long asm_rdtsc(void){
        return read_counter();
    }
#else
    static INLINE unsigned long asm_rdtsc(void){
        printf("error : asm_rdtsc can`t suppport  architecture\n");
        return 0;
    }
#endif

#define COUNTCYCLE_START(node) \
    pcm_hrtime_t start_##node;\
    pcm_hrtime_t stop_##node;\
    start_##node = asm_rdtsc ();

#define COUNTCYCLE_END(node,time)\
    stop_##node = asm_rdtsc (); \
    time = CYCLE2NS (stop_##node - start_##node); \
    printf(#node " time :%ld \n", time);

#endif
