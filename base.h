#ifndef __BASE_H__
#define __BASE_H__

#if defined (__GNUC__) || defined (__clang__) || defined(__INTEL_COMPILER)
    #define LIKELY(condition) __builtin_expect(!!(condition), 1)
    #define UNLIKELY(condition) __builtin_expect(!!(condition), 0)
    #define RESTRICT __restrict__

    #ifndef __ARM_FEATURE_SVE
        #define INLINE __attribute__((always_inline))
    #else
        // This is a workaround of a bug in armclang
        #define INLINE
    #endif

    #ifndef __arm__
        #define ALIGNED(x) __attribute__((aligned(x)))
    #else
        #define ALIGNED(x)
    #endif

    #ifndef __INTEL_COMPILER
        #define CONST const
    #else
        #define CONST
    #endif

    #if defined(__MINGW32__) || defined(__MINGW64__) || defined(__CYGWIN__)
        #define EXPORT __stdcall __declspec(dllexport)
    #else
        #define EXPORT __attribute__((visibility("default")))
    #endif

    #ifdef INFINITY
        #undef INFINITY
    #endif

    #ifdef NAN
        #undef NAN
    #endif

    #define NAN __builtin_nan("")
    #define NANf __builtin_nanf("")
    #define NANl __builtin_nanl("")
    #define INFINITY __builtin_inf()
    #define INFINITYf __builtin_inff()
    #define INFINITYl __builtin_infl()

    #if defined(__INTEL_COMPILER)
        #define INFINITYq __builtin_inf()
        #define NANq __builtin_nan("")
    #else
        #define INFINITYq __builtin_infq()
        #define NANq (INFINITYq - INFINITYq)
    #endif

#elif defined(_MSC_VER)

    #define INLINE __forceinline
    #define CONST
    #define EXPORT __declspec(dllexport)
    #define RESTRICT
    #define ALIGNED(x)
    #define LIKELY(condition) (condition)
    #define UNLIKELY(condition) (condition)

    #define INFINITYf ((float)INFINITY)
    #define NANf ((float)NAN)
    #define INFINITYl ((long double)INFINITY)
    #define NANl ((long double)NAN)
    #define INFINITY (1e+300 * 1e+300)
    #define NAN (INFINITY - INFINITY)

    static INLINE CONST int isinff(float x) { return x == INFINITYf || x == -INFINITYf; }
    static INLINE CONST int isinfl(long double x) { return x == INFINITYl || x == -INFINITYl; }
    static INLINE CONST int isnanf(float x) { return x != x; }
    static INLINE CONST int isnanl(long double x) { return x != x; }

#endif // defined(_MSC_VER)

#ifdef __APPLE__
    static INLINE CONST int isinff(float x) { return x == INFINITYf || x == -INFINITYf; }
    static INLINE CONST int isinfl(long double x) { return x == INFINITYl || x == -INFINITYl; }
    static INLINE CONST int isnanf(float x) { return x != x; }
    static INLINE CONST int isnanl(long double x) { return x != x; }
#endif
#endif // #ifndef __BASE_H__
